//
//  infocell.swift
//  RobinApp
//
//  Created by mac on 05/07/19.
//  Copyright © 2019 com.assignment.robin. All rights reserved.
//

import UIKit

class infocell: UITableViewCell {
    
    let formatString101 = NSLocalizedString("Save",
                                            comment: "Save")
    
    let formatString102 = NSLocalizedString("Note",
                                            comment: "Note")
    
    @IBOutlet weak var notetextfield: UITextField!
    
    @IBOutlet weak var expensetype: UILabel!
    
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var material: UILabel!

    @IBOutlet weak var savebutton: UIButton!
    
    @IBOutlet weak var cancelbutton: UIButton!
    
    func setcell(expenselist : expenselist){
        expensetype.text = expenselist.expensetype
        material.text = expenselist.material
        textfield.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        textfield.layer.borderWidth = 1
        note.text = String.localizedStringWithFormat(formatString102)
        savebutton.setTitle(String.localizedStringWithFormat(formatString101), for: .normal)
        }
    
    
       
  
    
    

}
