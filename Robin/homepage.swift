//
//  homepage.swift
//  Robin
//
//  Created by mac on 09/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit
import MobileCoreServices
import TesseractOCR





class homepage: UIViewController {
    var newString = ""
    var newString1 = ""
    var str = ""
    var open : Bool = true
    let a = UIView()
    var nameText = ""
    var fullNameArr = [String]()
    var companyname = ""
    var paymenttype = ""
    var transportexpense = ""
    var entertainmentexpense = ""
    var totalexpense = ""
    var otherexpense = ""
    var tax = ""
    var date = ""
    var storename = ""
    var siteexpenses = ""
    var commonSiteexpenses = ""
    var expandableexpenses = ""
    var welfareexpenses = ""
    var gasoline = ""
    var Amount = ""
    var paymentdestination = ""
    var payment = ""
    var phonenumber = ""
    var note = ""
    @IBOutlet weak var clickhere: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sidebutton: UIButton!
    @IBOutlet weak var L2: UILabel!
    @IBOutlet weak var L1: UILabel!
    @IBOutlet weak var subview3image: UIImageView!
    @IBOutlet weak var subview2image: UIImageView!
    @IBOutlet weak var subimage: UIImageView!
    @IBOutlet weak var subsideview3: UIView!
    @IBOutlet weak var subsideview2: UIView!
    @IBOutlet weak var subsideview1: UIView!
    @IBOutlet weak var mainsideview: UIView!
    @IBOutlet weak var cameraview: UIImageView!
    var seperatedlines = [String]()
    let formatString27 = NSLocalizedString("General Management Cost",comment: "")
    
    let formatString28 = NSLocalizedString("Site Material",comment: "")

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.hideKeyboardWhenTappedAround()
        
        let formatString13 = NSLocalizedString("Click here to start detecting receipts",
                                              comment: "Click here to start detecting receipts")
        clickhere.text = String.localizedStringWithFormat(formatString13)
        
        let formatString2 = NSLocalizedString("Order History",
                                              comment: "Order history label")
        
        let formatString3 = NSLocalizedString("Log Out",
                                              comment: "log out")
        
        L1.text = String.localizedStringWithFormat(formatString2)
        L2.text = String.localizedStringWithFormat(formatString3)
        cameraview.clipsToBounds = true
        cameraview.layer.cornerRadius = 10
        view.addSubview(mainsideview)
        mainsideview.frame = CGRect(x: 0-self.view.frame.width/2, y: 80, width: self.view.frame.width/2, height: self.view.frame.height-80)
        mainsideview.addSubview(subsideview1)
        subsideview1.frame = CGRect(x: 0, y: 0, width: mainsideview.frame.width, height: mainsideview.frame.width)
        subsideview1.addSubview(subimage)
        subimage.frame = CGRect(x:15 , y:10 , width: subsideview1.frame.width-35, height: subsideview1.frame.height-30)
        subimage.clipsToBounds = true
        subimage.layer.cornerRadius = subimage.frame.width/2
        mainsideview.addSubview(subsideview2)
        mainsideview.addSubview(subsideview3)
        subsideview2.frame = CGRect(x: 0, y: mainsideview.frame.width, width: mainsideview.frame.width, height: 50)
        subsideview3.frame = CGRect(x: 0, y: mainsideview.frame.width+50, width: mainsideview.frame.width, height: 50)
        subsideview2.addSubview(subview2image)
        subsideview3.addSubview(subview3image)
        subview2image.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        subview3image.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        subsideview2.addSubview(L1)
        subsideview3.addSubview(L2)
        L1.frame = CGRect(x: 55, y: subsideview2.frame.height/2-10, width: subsideview2.frame.width-55, height: 20)
        L2.frame = CGRect(x: 55, y: subsideview3.frame.height/2-10, width: subsideview3.frame.width-55, height: 20)
        
        let editprofilelbl = UILabel()
        subsideview1.addSubview(editprofilelbl)
        
        let formatString = NSLocalizedString("Edit Profile",
                                             comment: "Edit Profile")
        editprofilelbl.text = String.localizedStringWithFormat(formatString)
        editprofilelbl.frame = CGRect(x: 0, y: subsideview1.frame.height-30, width: subsideview1.frame.width, height: 20)
        editprofilelbl.textAlignment = NSTextAlignment.center
        editprofilelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        
    }
    

    @IBAction func clicksidebutton(_ sender: Any) {
        if (open == true){
            UIView.animate(withDuration: 0.5,  animations: {
                self.mainsideview.frame = CGRect(x: 0, y: 80, width: self.view.frame.width/2, height: self.view.frame.height-80)
            }, completion: nil)
            open = false
        }else if (open == false){
            UIView.animate(withDuration: 0.5, animations: {
                self.mainsideview.frame = CGRect(x: 0-self.view.frame.width/2, y: 80, width: self.view.frame.width/2, height: self.view.frame.height-80)
            }, completion: nil)
            open = true
        }
        }
    
    
    @IBAction func doscanbutton(_ sender: Any) {
        getCamera()
    }
    
    
    func getCamera(){
        let imagePickerActionSheet =
            UIAlertController(title: "Snap/Upload Image",
                              message: nil,
                              preferredStyle: .actionSheet)
        
        // 2
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let formatString6 = NSLocalizedString("Take Photo",comment: "Take Photo")
            let cameraButton = UIAlertAction(title: String.localizedStringWithFormat(formatString6),
                                             style: .default) { (alert) -> Void in
                                                // 1
                                                self.activityIndicator.startAnimating()
                                                // 2
                                                let imagePicker = UIImagePickerController()
                                                // 3
                                                imagePicker.delegate = self
                                                // 4
                                                imagePicker.sourceType = .camera
                                                // 5
                                                imagePicker.mediaTypes = [kUTTypeImage as String]
                                                // 6
                                                self.present(imagePicker, animated: true, completion: {
                                                    // 7
                                                    self.activityIndicator.stopAnimating()
                                                })
                                                
            }
            imagePickerActionSheet.addAction(cameraButton)
        }
        
        
        let formatString4 = NSLocalizedString("Choose Existing",
                                              comment: "Choose Existing")
        
        let libraryButton = UIAlertAction(
            title: String.localizedStringWithFormat(formatString4),
            style: .default) { (alert) -> Void in
                self.activityIndicator.startAnimating()
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: {
                    self.activityIndicator.stopAnimating()
                })
                
        }
        imagePickerActionSheet.addAction(libraryButton)
        let formatString7 = NSLocalizedString("Cancel",comment: "Cancel")
        let cancelButton = UIAlertAction(title: String.localizedStringWithFormat(formatString7), style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
        present(imagePickerActionSheet, animated: true)
        
    }
    
    
    
    
    
    func performImageRecognition(_ image: UIImage) {
        
        
        let scaledImage = image.scaledImage(1000) ?? image
        
        
        // 1
        if let tesseract = G8Tesseract(language: "jpn+eng") {
            // 2
            tesseract.engineMode = .tesseractOnly
            // 3
            tesseract.pageSegmentationMode = .auto
            // 4
            tesseract.image = scaledImage
            // 5
            tesseract.recognize()
            // 6
            nameText = tesseract.recognizedText!
            
            str = tesseract.recognizedText!
            
            seperatedlines = str.components(separatedBy: "\n")
            let miyukistring = seperatedlines[0].components(separatedBy: " ")
            
            if  seperatedlines[0] == "EneJ'et"{enerjet()}
            
            else if miyukistring[0] == "No." {
              miyuki()
            
            }else{
               getalert()
                
             //  performSegue(withIdentifier: "ocr", sender: self)
                
            }
            }
        activityIndicator.stopAnimating()
    }
    func getalert(){
        let alert = UIAlertController(title: "OOPS!", message: "The Reciept you want to scan is not Appears according to the format.But you are welcome to Fill the Sheet Manually.", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            self.performSegue(withIdentifier: "transfer", sender: self)
        })
        
        
        let Cancel = UIAlertAction(title: "Cancel", style: .default, handler:nil)
        alert.addAction(ok)
        alert.addAction(Cancel)
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func enerjet(){
       
        let line1 = seperatedlines[0]
        companyname = line1
        
        let line2 = seperatedlines[10]
        payment = line2
        let line3 = seperatedlines[20]
        let space3 = line3.components(separatedBy: " ")
        Amount = space3[1]
        
        let line6 = seperatedlines[19]
        
        
        
        let space4 = line6.components(separatedBy: " ")
        tax = space4[1]
        
        let line4 = seperatedlines[32]
        date = line4
        
        let line5 = seperatedlines[7]
        phonenumber = line5
        
        welfareexpenses = "0"
        expandableexpenses = "0"
        siteexpenses = "0"
        commonSiteexpenses = "0"
        gasoline = "0"
        totalexpense = "0"
        entertainmentexpense = "0"
        transportexpense = "0"
        paymenttype = String.localizedStringWithFormat(formatString28)
        otherexpense = "0"
        phonenumber = seperatedlines[23]
        note = "???"
        
        performSegue(withIdentifier: "transfer", sender: self)
        
        
        
        //  performSegue(withIdentifier: "ocr", sender: self)
        
    }
    func miyuki(){
        let line1 = seperatedlines[1]
        let companyArr = line1.components(separatedBy: " ")
        let line2 = seperatedlines[7]
        let AmountArr = line2.components(separatedBy: " ")
        let line3 = seperatedlines[9]
        let TaxArr = line3.components(separatedBy: " ")
        
        tax = TaxArr[2]
        companyname = companyArr[1]
        Amount = AmountArr[1]
        storename = seperatedlines[18]
        paymentdestination = seperatedlines[20] + seperatedlines[21]
        payment = String.localizedStringWithFormat(formatString27)
        date = seperatedlines[3]
        welfareexpenses = "0"
        expandableexpenses = "0"
        siteexpenses = "0"
        commonSiteexpenses = "0"
        gasoline = "0"
        totalexpense = "0"
        entertainmentexpense = "0"
        transportexpense = "0"
        paymenttype = String.localizedStringWithFormat(formatString28)
        otherexpense = "0"
        phonenumber = seperatedlines[23]
        note = "???"
        
        
        
      performSegue(withIdentifier: "transfer", sender: self)
  //    performSegue(withIdentifier: "ocr", sender: self)

        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transfer"{ let vc = segue.destination as! afterScan
            vc.detectedcompanyname = companyname
            vc.date = date
            vc.store = storename
            vc.Amount = Amount
            vc.paymentdestination = paymentdestination
            vc.payment = payment
            vc.phonenumber = phonenumber
            vc.tax = tax
            vc.otherexpenses = otherexpense
            vc.entertainmentexpense = entertainmentexpense
            vc.totalexpense = totalexpense
            vc.paymenttype = paymenttype
            vc.welfareexpenses = welfareexpenses
            vc.expandableexpenses = expandableexpenses
            vc.commonSiteexpenses = commonSiteexpenses
            vc.siteexpenses = siteexpenses
            vc.gasoline = gasoline
            vc.note = note
            
        }
        if segue.identifier == "ocr"{let vc = segue.destination as! scanner
            vc.finalName = str
            
        }
    }
    
    

}

extension homepage: UINavigationControllerDelegate {
    
    
}
extension homepage: UIImagePickerControllerDelegate {
    // 3
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // 1
        guard let selectedPhoto =
            info[.originalImage] as? UIImage else {
                dismiss(animated: true)
                return
        }
        // 2
        activityIndicator.startAnimating()
        // 3
        dismiss(animated: true) {
            self.performImageRecognition(selectedPhoto)
        }
        
    }
    
    
    
    
}

// MARK: - UIImage extension

//1
extension UIImage {
    // 2
    func scaledImage(_ maxDimension: CGFloat) -> UIImage? {
        // 3
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        // 4
        if size.width > size.height {
            scaledSize.height = size.height / size.width * scaledSize.width
        } else {
            scaledSize.width = size.width / size.height * scaledSize.height
        }
        // 5
        UIGraphicsBeginImageContext(scaledSize)
        draw(in: CGRect(origin: .zero, size: scaledSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        // 6
        return scaledImage
    }
}
extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
