//
//  Cell.swift
//  Dr plus
//
//  Created by mac on 11/06/19.
//  Copyright © 2019 com.assignment.auction. All rights reserved.
//

import UIKit

class Cell: UITableViewCell {

    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var shop: UILabel!
    @IBOutlet weak var price: UILabel!

func setCell(list: list){
    orderId.text = list.orderid
    date.text = list.date
    shop.text = list.shop
    price.text = list.price
    
    
}
}
