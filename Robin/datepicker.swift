import UIKit

class datepicker: UIViewController {
    var Amount = ""
    var payment = ""
    var phonenumber = ""
    var companyname = ""
    var paymenttype = ""
    var transportexpense = "0"
    var entertainmentexpense = "0"
    var totalexpense = "0"
    var tax = ""
    var date = ""
    var Address = ""
    var storename = ""
    var note = ""
    var otherexpense = "0"
    var commonSiteexpenses = "0"
    var expandableexpenses = "0"
    var gasoline = "0"
    var siteexpenses = "0"
    var welfareexpenses = "0"
    
    
    
    
    @IBOutlet weak var companyheading: UILabel!
    @IBOutlet weak var contactlbl: UILabel!
    @IBOutlet weak var contacttextfield: UITextField!
    @IBOutlet weak var taxtextfield: UITextField!
    @IBOutlet weak var taxlbl: UILabel!
    @IBOutlet weak var companylbl: UILabel!
    @IBOutlet weak var storenameTV: UITextView!
    @IBOutlet weak var AddressTextView: UITextView!
    @IBOutlet weak var paymentbutton: UIButton!
    @IBOutlet weak var Amountlbl: UILabel!
    @IBOutlet weak var Amounttextfield: UITextField!
    @IBOutlet weak var haveyou: UILabel!
    @IBOutlet weak var contactHeading: UILabel!
    @IBOutlet weak var selectComapany: UILabel!
    @IBOutlet weak var DateofPurchase: UILabel!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var paymentDestination: UILabel!
    @IBOutlet weak var enterAmount: UILabel!
    @IBOutlet weak var Taxheading: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var Sitematerial: UIButton!
    @IBOutlet weak var generalmanagment: UIButton!
    @IBOutlet weak var paymenttypeheading: UILabel!
    @IBOutlet weak var submit: UIButton!
    var companyArray: [String] = []
    
    let formatString25 = NSLocalizedString("No",comment: "No")
    let formatString20 = NSLocalizedString("Robin",comment: "Robin")
    let formatString17 = NSLocalizedString("Miyuki",comment: "Miyuki")
    let formatString18 = NSLocalizedString("EneJet",comment: "Robin maintenance")
    let formatString19 = NSLocalizedString("Robin Trust",comment: "Robin Trust")
    let formatString26 = NSLocalizedString("Yes",comment: "Yes")
    let formatString27 = NSLocalizedString("General Management Cost",comment: "General Management Cost")
    let formatString28 = NSLocalizedString("Site Expenses",comment: "Transportation Expenses")
    let formatString29 = NSLocalizedString("Entertainment Expenses",comment: "Entertainment Expenses")
    let formatString30 = NSLocalizedString("Other Expenses",comment: "Other Expenses")
    let formatString31 = NSLocalizedString("Total Expenses",comment: "Total Expenses")
    let formatString32 = NSLocalizedString("Contact",comment: "Contact")
    let formatString33 = NSLocalizedString("Select Company",comment: "Select Company")
    let formatString34 = NSLocalizedString("Date of Purchase",comment: "Date of Purchase")
    let formatString35 = NSLocalizedString("Store name",comment: "Store name")
    let formatString36 = NSLocalizedString("Store Address",comment: "Store Address")
    let formatString37 = NSLocalizedString("Enter Amount",comment: "Enter Amount")
    let formatString38 = NSLocalizedString("Tax",comment: "Tax")
    let formatString39 = NSLocalizedString("Expenses",comment: "Expenses")
    let formatString40 = NSLocalizedString("Company Name",comment: "Company Name")
    let formatString41 = NSLocalizedString("Payment Type",comment: "Payment Type")
    let formatString42 = NSLocalizedString("Submit",comment: "Submit")
    
     let formatString43 = NSLocalizedString("Expenses",comment: "Expenses")
    
    
    @IBOutlet weak var Expensesbutton: UIButton!
    
   
    
override func viewDidLoad() {
        super.viewDidLoad()
    
    
     self.hideKeyboardWhenTappedAround()
    
    submit.setTitle(String.localizedStringWithFormat(formatString42), for: .normal)
    
    Expensesbutton.setTitle(String.localizedStringWithFormat(formatString43), for: .normal)
        paymenttypeheading.text = String.localizedStringWithFormat(formatString41)
        companyheading.text = String.localizedStringWithFormat(formatString40)
        selectComapany.text = String.localizedStringWithFormat(formatString33)
        DateofPurchase.text = String.localizedStringWithFormat(formatString34)
        storeName.text = String.localizedStringWithFormat(formatString35)
        paymentDestination.text = String.localizedStringWithFormat(formatString36)
        enterAmount.text = String.localizedStringWithFormat(formatString37)
        Taxheading.text = String.localizedStringWithFormat(formatString38)
        contactHeading.text = String.localizedStringWithFormat(formatString32)
        Sitematerial.setTitle(String.localizedStringWithFormat(formatString28), for: .normal)
        generalmanagment.setTitle(String.localizedStringWithFormat(formatString27), for: .normal)
        contactlbl.text = phonenumber
        contacttextfield.text = phonenumber
        taxlbl.text = tax
        Sitematerial.isSelected = true
        companylbl.text = companyname
        storenameTV.text = storename
        AddressTextView.text = Address
        paymentbutton.isSelected = true
        datelbl.text = date
        Amounttextfield.text = Amount
        Amountlbl.text = Amounttextfield.text
        paymentbutton.titleLabel?.text = payment
        taxtextfield.text = tax
        companyArray = [String.localizedStringWithFormat(formatString20),String.localizedStringWithFormat(formatString17),String.localizedStringWithFormat(formatString18),String.localizedStringWithFormat(formatString19)]
        
    }
    
    @IBAction func taxchange(_ sender: Any) {
        taxlbl.text = taxtextfield.text
        tax = taxtextfield.text!
    }
    
    @IBAction func contactchnge(_ sender: UITextField) {
        phonenumber = contacttextfield.text!
        contactlbl.text = contacttextfield.text
    }
    
    @IBAction func change(_ sender: UITextField) {
        let editedAmount = Amounttextfield.text
        Amountlbl.text = editedAmount
    }
    
  
    
    @IBAction func paymentclicked(_ sender: Any) {
        if paymentbutton.isSelected == true{
            paymentbutton.isSelected = false
            payment = String.localizedStringWithFormat(formatString25)
            }else{
            paymentbutton.isSelected = true
            payment = paymentbutton.titleLabel!.text!
            payment = String.localizedStringWithFormat(formatString26)}
        
    }
    
    @IBAction func siteclicked(_ sender: Any) {
        if Sitematerial.isSelected == true{
            generalmanagment.isSelected = true
            Sitematerial.isSelected = false
            paymenttype = String.localizedStringWithFormat(formatString27)
        }else{
            generalmanagment.isSelected = false
            Sitematerial.isSelected = true
            paymenttype = String.localizedStringWithFormat(formatString28)}
        }
    
    @IBAction func generalclicked(_ sender: Any) {
        if generalmanagment.isSelected == false{
        generalmanagment.isSelected = true
        Sitematerial.isSelected = false
        paymenttype = String.localizedStringWithFormat(formatString27)
            }else{
            generalmanagment.isSelected = false
            Sitematerial.isSelected = true
            paymenttype = String.localizedStringWithFormat(formatString28)}
        }
    
    @IBAction func gotoExpense(_ sender: Any) {
        
        performSegue(withIdentifier: "exg", sender: self)
    }
    @IBAction func submit(_ sender: Any) {
        performSegue(withIdentifier: "datesegue", sender: self)
    }
    
    @IBAction func datechangeaction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        let strDate = dateFormatter.string(from: datepicker.date)
        datelbl.text = strDate
        date = strDate
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "datesegue"{ let vc = segue.destination as! afterScan
            vc.date = datelbl.text!
            vc.detectedcompanyname = companyname
            vc.Amount = Amountlbl.text!
            vc.payment = payment
            Address = AddressTextView.text
            vc.paymentdestination = Address
            storename = storenameTV.text
            vc.store = storename
            vc.otherexpenses = otherexpense
            vc.entertainmentexpense = entertainmentexpense
            vc.totalexpense = totalexpense
            vc.tax = tax
            vc.phonenumber = phonenumber
            vc.paymenttype = paymenttype
            vc.phonenumber = phonenumber
            vc.welfareexpenses = welfareexpenses
            vc.expandableexpenses = expandableexpenses
            vc.commonSiteexpenses = commonSiteexpenses
            vc.siteexpenses = siteexpenses
            vc.gasoline = gasoline
            vc.note = note
            }
        
        if segue.identifier == "exg"{ let vc = segue.destination as! expenseTV
            vc.date = datelbl.text!
            vc.companyname = companyname
            vc.Amount = Amountlbl.text!
            vc.payment = payment
            Address = AddressTextView.text
            vc.Address = Address
            vc.storename = Address
            storename = storenameTV.text
            vc.storename = storename
            vc.otherexpense = otherexpense
            vc.paymenttype = paymenttype
            vc.entertainmentexpense = entertainmentexpense
            vc.transportexpense = transportexpense
            vc.totalexpense = totalexpense
            vc.tax = tax
            vc.phonenumber = phonenumber
            vc.siteexpenses = siteexpenses
            vc.commonSiteexpenses = commonSiteexpenses
            vc.expandableexpenses = expandableexpenses
            vc.otherexpense = otherexpense
            vc.note = note
        }
    }
    

    
    @IBAction func clickenglish(_ sender: Any) {
        languageButtonAction2()
    }
    
    @IBAction func clickjapanese(_ sender: Any) {
        languageButtonAction()    }
    
    
}
extension datepicker : UITableViewDataSource,UITableViewDelegate{
    
    
   func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return companyArray.count
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "companycell", for: indexPath)
        
        cell.textLabel!.text = self.companyArray[indexPath.row]
    companyname = self.companyArray[indexPath.row]
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            companyname = companyArray[indexPath.row]
        companylbl.text = companyname
        
    }
}
