//
//  CompanyTableViewController.swift
//  Robin
//
//  Created by mac on 12/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit

class CompanyTableViewController: UITableViewController {
     var companyArray: [String] = []
    let formatString20 = NSLocalizedString("Robin",comment: "Robin")
    let formatString17 = NSLocalizedString("Robin management",comment: "Robin management")
    let formatString18 = NSLocalizedString("Robin maintenance",comment: "Robin maintenance")
    let formatString19 = NSLocalizedString("Robin Trust",comment: "Robin Trust")

    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.hideKeyboardWhenTappedAround()
        
        companyArray = [String.localizedStringWithFormat(formatString20),String.localizedStringWithFormat(formatString17),String.localizedStringWithFormat(formatString18),String.localizedStringWithFormat(formatString19)]
       
    }

}
