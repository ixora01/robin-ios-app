//
//  editProfile.swift
//  Robin
//
//  Created by mac on 09/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit

class editProfile: UIViewController {
    @IBOutlet weak var fullname: UITextField!
    
    @IBOutlet weak var save: UIButton!
    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var changePassword: UITextField!
    @IBOutlet weak var malelbl: UILabel!
    @IBOutlet weak var femalebutton: UIButton!
    @IBOutlet weak var malebutton: UIButton!
    @IBOutlet weak var plusicon: UIImageView!
    @IBOutlet weak var ProfilePic: UIImageView!
    @IBOutlet weak var genderlbl: UILabel!
    @IBOutlet weak var femalelbl: UILabel!
    @IBOutlet weak var phone: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.hideKeyboardWhenTappedAround()
        
        
        let formatString = NSLocalizedString("female",
                                             comment: "female")
        
        let formatString14 = NSLocalizedString("Change Password",
                                             comment: "Change Password")
        
        changePassword.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString14))
        
        
        let formatString15 = NSLocalizedString("Email ID",
                                               comment: "Email ID")
        
        emailID.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString15))
        let formatString18 = NSLocalizedString("male",
                                              comment: "male")
        
        malelbl.text = String.localizedStringWithFormat(formatString18)
        
        let formatString4 = NSLocalizedString("Phone number",
                                              comment: "Phone number")
        
        let formatString2 = NSLocalizedString("gender",
                                              comment: "gender")
        
        let formatString3 = NSLocalizedString("full name",
                                              comment: "full name")
        
        fullname.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString3))
        
        
        
        phone.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString4))
        
        
        let formatString50 = NSLocalizedString("Save",
                                              comment: "Save")
        
        
        save.setTitle(String.localizedStringWithFormat(formatString50), for: .normal)
        
        
        
        
        
        
        
        femalelbl.text = String.localizedStringWithFormat(formatString)
        genderlbl.text = String.localizedStringWithFormat(formatString2)
        
        
        ProfilePic.clipsToBounds=true
        ProfilePic.layer.cornerRadius = ProfilePic.frame.width/2
        plusicon.clipsToBounds=true
        plusicon.layer.cornerRadius = 10
        
    }
    
    
    @IBAction func clickmale(_ sender: Any) {
        if femalebutton.isSelected {
            femalebutton.isSelected = false
            malebutton.isSelected = true
        }else{
            malebutton.isSelected = true
        }
    }
    
    @IBAction func clickfemale(_ sender: Any) {
        
        if malebutton.isSelected {
            malebutton.isSelected = false
            femalebutton.isSelected = true
        }else{
            femalebutton.isSelected = true
        }
    }
    

}
