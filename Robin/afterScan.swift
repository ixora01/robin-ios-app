

import UIKit

class afterScan: UIViewController {
    
    @IBOutlet weak var T1: UITableView!
    let datepicker = UIDatePicker()
    let blackview = UIView()
    var vcview = UIView()
    let companyTV = UITableView()
    var tax = ""
    var phonenumber = ""
    var detectedcompanyname = ""
    var siteexpenses = "0"
    var commonSiteexpenses = "0"
    var expandableexpenses = "0"
    var gasoline = ""
    var date = ""
    var store = ""
    var payment = ""
    var note = ""
    var paymenttype = ""
    var entertainmentexpense = "0"
    var otherexpenses = "0"
    var welfareexpenses = "0"
    var totalexpense = "0"
    var paymentdestination = ""
    var Amount = "0"
    var iscollapse = false
    var selectedindex = 0
    var listItem: [listarray1] = []
    var companyArray: [String] = []
    let formatString21 = NSLocalizedString("Site expenses",comment: "Site expenses")
    let formatString22 = NSLocalizedString("Tanaka House Material",comment: "Tanaka House Material")
    let formatString23 = NSLocalizedString("Gasoline",comment: "Gasoline")
    let formatString24 = NSLocalizedString("General Management Expenses",comment: "General Management Expenses")
    
    let formatString25 = NSLocalizedString("Save",comment: "Save")
    let formatString26 = NSLocalizedString("Cancel",comment: "Cancel")

    override func viewDidLoad() {
        super.viewDidLoad()
         self.hideKeyboardWhenTappedAround()
        listItem = createArray()
        T1.delegate = self
        T1.dataSource = self
    }
    @IBAction func editbuttonAction(_ sender: Any) {datetransfer()}
    
    
    @IBAction func savebuttonaction(_ sender: Any) {alert()}
    func alert(){
        let alert = UIAlertController(title: "", message: "Data Saved Successfully", preferredStyle: .actionSheet)
        present(alert, animated: true) {
            let duration: Double = 1
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                alert.dismiss(animated: true)
                }
           
            
        }
    }
    func datetransfer() {
        performSegue(withIdentifier: "reverse", sender: self)}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "reverse"{let vc = segue.destination as! datepicker
            vc.date = date
            vc.Amount = Amount
            vc.Address = paymentdestination
            vc.storename = store
            vc.payment = payment
            vc.companyname = detectedcompanyname
            vc.totalexpense = totalexpense
            vc.tax = tax
            vc.paymenttype = paymenttype
            vc.entertainmentexpense = entertainmentexpense
            vc.otherexpense = otherexpenses
            vc.phonenumber = phonenumber
            vc.otherexpense = otherexpenses
            vc.totalexpense = totalexpense
            vc.welfareexpenses = welfareexpenses
            vc.expandableexpenses = expandableexpenses
            vc.commonSiteexpenses = commonSiteexpenses
            vc.siteexpenses = siteexpenses
            vc.gasoline = gasoline
            vc.note = note
            }
    }
    
    
 
    func createArray() -> [listarray1] {
        var listitem: [listarray1] = []
        
        let formatString1 = NSLocalizedString("Company Name",comment: "Company Name")
        let formatString3 = NSLocalizedString("Date",comment: "Date")
        let formatString4 = NSLocalizedString("Store",comment: "Store")
        let formatString5 = NSLocalizedString("Payment",comment: "Payment")
        let formatString7 = NSLocalizedString("Payment destination",comment: "Payment destination")
        let formatString8 = NSLocalizedString("Contact",comment: "Seven-Eleven")
        let formatString9 = NSLocalizedString("Amount(tax included)",comment: "Amount(tax included)")
        let formatString10 = NSLocalizedString("Tax",comment: "Tax")
        let formatString11 = NSLocalizedString("Site Expenses",comment: "Site Expenses")
        let formatString12 = NSLocalizedString("Common Site Expenses",comment: "")
        let formatString13 = NSLocalizedString("Gasoline",comment: "Gasoline")
        let formatString14 = NSLocalizedString("Welfare Expenses",comment: "")
        let formatString19 = NSLocalizedString("Payment type",comment: "payment type")
        let formatString15 = NSLocalizedString("Expandable Expenses",comment: "Expandable")
        let formatString16 = NSLocalizedString("Entertainment Expenses",comment: "Entertainment")
        let formatString17 = NSLocalizedString("Other Expenses",comment: "Entertainment")
        let formatString18 = NSLocalizedString("Note",comment: "Note")
        
        
        
        
        let list1 = listarray1(image: #imageLiteral(resourceName: "1"), title: String.localizedStringWithFormat(formatString1), value: detectedcompanyname)
        let list2 = listarray1(image: #imageLiteral(resourceName: "2"), title: String.localizedStringWithFormat(formatString3), value: date)
        let list3 = listarray1(image: #imageLiteral(resourceName: "image5"), title: String.localizedStringWithFormat(formatString4), value: store)
        let list4 = listarray1(image: #imageLiteral(resourceName: "4"), title: String.localizedStringWithFormat(formatString5), value: payment)
        let list5 = listarray1(image: #imageLiteral(resourceName: "3"), title: String.localizedStringWithFormat(formatString7), value: paymentdestination)
        let list6 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString9), value: Amount)
        let list7 = listarray1(image: #imageLiteral(resourceName: "android"), title: String.localizedStringWithFormat(formatString8), value: phonenumber)
        let list8 = listarray1(image: #imageLiteral(resourceName: "image2"), title: String.localizedStringWithFormat(formatString10), value: tax)
        let list16 = listarray1(image: #imageLiteral(resourceName: "image2"), title: String.localizedStringWithFormat(formatString19), value: paymenttype)
        let list9 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString11), value: siteexpenses)
        let list10 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString12), value: commonSiteexpenses)
        let list11 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString13), value: gasoline)
        let list12 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString14), value: welfareexpenses)
        let list13 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString15), value: expandableexpenses)
        let list14 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString16), value: entertainmentexpense)
        let list15 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString17), value: otherexpenses)
        let list18 = listarray1(image: #imageLiteral(resourceName: "note"), title: String.localizedStringWithFormat(formatString18), value: note)
        
        listitem.append(list1)
        listitem.append(list2)
        listitem.append(list3)
        listitem.append(list4)
        listitem.append(list5)
        listitem.append(list6)
        listitem.append(list7)
        listitem.append(list8)
        listitem.append(list16)
        listitem.append(list9)
        listitem.append(list18)
        listitem.append(list10)
        listitem.append(list11)
        listitem.append(list12)
        listitem.append(list13)
        listitem.append(list14)
        listitem.append(list15)
        
        return listitem
        
    }
}
extension afterScan : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
            let listitem = listItem[indexPath.row]
            let Cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell1")as! Cell1
            Cell1.setCell1(listarray1: listitem)
            return Cell1
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return listItem.count
       }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
            return 50
        }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
            if indexPath.row == 0{}
            if indexPath.row == 1{}
            if indexPath.row == 3{}
            if indexPath.row == 7{}
        
        if indexPath.row == 2{
          
            
        }
        
    }
}

