import UIKit

class purchaseHistory: UIViewController{
    var listItem: [list] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var tapbutton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listItem = createArray()
         self.hideKeyboardWhenTappedAround()
        
        
    }
    
    func createArray() -> [list] {
        var listitem: [list] = []
        
        let list1 = list(orderid: "537437498", date: "07/08/2018", shop: "5334 shop", price: "4000")
        let list2 = list(orderid: "537437498", date: "9/08/2018", shop: "53090shop", price: "9000")
        
        
        
        listitem.append(list1)
        listitem.append(list2)
        
        
        
        return listitem
    }
    
}

extension purchaseHistory: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listitem = listItem[indexPath.row]
        
        let Cell = tableView.dequeueReusableCell(withIdentifier: "Cell")as! Cell
        Cell.setCell(list: listitem)
        return Cell
    }
    
    
}
