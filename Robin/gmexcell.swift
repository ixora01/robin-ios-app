//
//  gmexcell.swift
//  Robin
//
//  Created by mac on 18/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit

class gmexcell: UITableViewCell {

    @IBOutlet weak var gasolinetf: UITextField!
    
    @IBOutlet weak var welfaretf: UITextField!

    @IBOutlet weak var expandablestf: UITextField!
    
    @IBOutlet weak var entertainmenttf: UITextField!
    
    @IBOutlet weak var othertextfield: UITextField!
    
    @IBOutlet weak var heading: UILabel!
    
    
    @IBOutlet weak var savebtn: UIButton!
    
    
    @IBOutlet weak var gaslbl: UILabel!
    
    
    @IBOutlet weak var wlflbl: UILabel!
    
    @IBOutlet weak var expndbl: UILabel!
    
    @IBOutlet weak var othrlbl: UILabel!
   
    @IBOutlet weak var enterlbl: UILabel!
}
