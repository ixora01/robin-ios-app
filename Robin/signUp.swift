import UIKit

class signUp: UIViewController {

  
    @IBOutlet weak var doyou: UIButton!
    @IBOutlet weak var genderlbl: UILabel!
    @IBOutlet weak var femalelbl: UILabel!
    @IBOutlet weak var malelbl: UILabel!
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var female: UIButton!
    @IBOutlet weak var male: UIButton!
    @IBOutlet weak var signup: UIButton!
   
    @IBOutlet weak var cellphonenumber: UITextField!
    @IBOutlet weak var emailid: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.hideKeyboardWhenTappedAround()

        let formatString = NSLocalizedString("Sign Up",
                                             comment: "Sign Up")
        
        let formatString19 = NSLocalizedString("Username",
                                             comment: "Username")
        
        let formatString20 = NSLocalizedString("Password",
                                               comment: "Password")
        
        
        let formatString1 = NSLocalizedString("email ID",
                                              comment: "email ID")
        
        emailid.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString1))
        
        username.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString19))
        
        password.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString20))
        
        
        
        let formatString15 = NSLocalizedString("Cell Phone number",
                                              comment: "Cell Phone number")
        
        
        
        let formatString16 = NSLocalizedString("male",
                                               comment: "male")
        
        let formatString17 = NSLocalizedString("female",
                                               comment: "female")
        
        
        
        let formatString18 = NSLocalizedString("gender",
                                               comment: "gender")
        
        malelbl.text = String.localizedStringWithFormat(formatString16)
        
        femalelbl.text = String.localizedStringWithFormat(formatString17)
        
        genderlbl.text = String.localizedStringWithFormat(formatString18)
        
        
        
        
        
        
        cellphonenumber.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString15))
        
        
        
        signup.setTitle(String.localizedStringWithFormat(formatString), for: .normal)
        
        
        let formatString25 = NSLocalizedString("Do you have an account? Sign in",
                                               comment: "Do you have an account? Sign in")
        
        
        doyou.setTitle(String.localizedStringWithFormat(formatString25), for: .normal)
    }
    

    @IBAction func clickmale(_ sender: Any) {
        if female.isSelected {
            female.isSelected = false
            male.isSelected = true
        }else{
            male.isSelected = true
        }
        
    }
    
    @IBAction func clickfemale(_ sender: Any) {
        if male.isSelected {
            male.isSelected = false
            female.isSelected = true
        }else{
            female.isSelected = true
        }
        
    }
    
}
