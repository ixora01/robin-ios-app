//
//  expenseTV.swift
//  Robin
//
//  Created by mac on 09/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit

class expenseTV: UIViewController {
    var Amount = ""
    var payment = ""
    var phonenumber = ""
    var companyname = ""
    var paymenttype = ""
    var transportexpense = "0"
    var entertainmentexpense = "0"
    var totalexpense = "0"
    var tax = ""
    var date = ""
    var Address = ""
    var storename = ""
    var otherexpense = "0"
    var commonSiteexpenses = "0"
    var expandableexpenses = "0"
    var gasolines = "0"
    var siteexpenses = "0"
    var welfareexpenses = "0"
    var note = ""
    
    
    
    @IBOutlet weak var siteexpense: UILabel!
    @IBOutlet weak var expenseTable: UITableView!
    
    @IBOutlet weak var gasoline: UILabel!
    
    @IBOutlet weak var welfareexp: UILabel!
    
    @IBOutlet weak var expandables: UILabel!
    
    @IBOutlet weak var entertainment: UILabel!
    
    @IBOutlet weak var siteexp: UILabel!
    
    @IBOutlet weak var commsiteexp: UILabel!
    
    @IBOutlet weak var otherexpenses: UILabel!
    
    @IBOutlet weak var Submit: UIButton!
   
    @IBOutlet weak var siteheading: UILabel!
    
    @IBOutlet weak var commheading: UILabel!
    
    @IBOutlet weak var gasheading: UILabel!
    
    @IBOutlet weak var welheading: UILabel!
    
    @IBOutlet weak var expndblheading: UILabel!
    
    @IBOutlet weak var enerheading: UILabel!
    
    @IBOutlet weak var otherheading: UILabel!
    
    var listItem: [expenselist] = []
    var listItem2: [expenselist2] = []
    var iscollapse: Bool = false
    var selectedIndex = -1
    
 
    @IBOutlet weak var Notelbl: UILabel!
    
    @IBAction func saveaction(_ sender: Any) {
        expenseTable.reloadData()
        iscollapse = false
        siteexpenses = siteexp.text!
    }
    
  
    @IBAction func info2save(_ sender: Any) {
        expenseTable.reloadData()
        iscollapse = false
        commonSiteexpenses = commsiteexp.text!
    }
    
    @IBAction func gmesave(_ sender: Any) {
        expenseTable.reloadData()
        iscollapse = false
        siteexpenses = siteexp.text!
        commonSiteexpenses = commsiteexp.text!
        gasolines = gasoline.text!
        otherexpense = otherexpenses.text!
        expandableexpenses = expandables.text!
        entertainmentexpense = entertainment.text!
        welfareexpenses = welfareexp.text!
        siteexpenses = siteexp.text!
        
        
    }
    
    
    @IBAction func backbutton(_ sender: Any) {
        performSegue(withIdentifier: "backtransfer", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backtransfer"{let vc = segue.destination as! datepicker
            siteexpenses = siteexp.text!
            commonSiteexpenses = commsiteexp.text!
            gasolines = gasoline.text!
            otherexpense = otherexpenses.text!
            expandableexpenses = expandables.text!
            entertainmentexpense = entertainment.text!
            welfareexpenses = welfareexp.text!
            siteexpenses = siteexp.text!
            vc.date = date
            vc.Amount = Amount
            vc.Address = Address
            vc.storename = storename
            vc.payment = payment
            vc.companyname = companyname
            vc.totalexpense = totalexpense
            vc.tax = tax
            vc.paymenttype = paymenttype
            vc.entertainmentexpense = entertainmentexpense
            vc.otherexpense = otherexpense
            vc.phonenumber = phonenumber
            vc.totalexpense = totalexpense
            vc.welfareexpenses = welfareexpenses
            vc.expandableexpenses = expandableexpenses
            vc.commonSiteexpenses = commonSiteexpenses
            vc.Address = Address
            vc.siteexpenses = siteexpenses
            vc.gasoline = gasolines
            expandables.text = expandableexpenses
            entertainment.text = entertainmentexpense
           otherexpenses.text = otherexpense
            noteTV.text = note
            vc.note = note
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.hideKeyboardWhenTappedAround()
        
        siteexp.text = siteexpenses
        commsiteexp.text = commonSiteexpenses
        gasoline.text = gasolines
        welfareexp.text = welfareexpenses
        expandables.text = expandableexpenses
        siteheading.text = String.localizedStringWithFormat(formatString20)
        commheading.text = String.localizedStringWithFormat(formatString23)
        gasheading.text = String.localizedStringWithFormat(formatString22)
        welheading.text = String.localizedStringWithFormat(formatString105)
        expndblheading.text = String.localizedStringWithFormat(formatString103)
        enerheading.text = String.localizedStringWithFormat(formatString102)
        otherheading.text = String.localizedStringWithFormat(formatString104)
       siteexpense.text = String.localizedStringWithFormat(formatString15)
        Notelbl.text = String.localizedStringWithFormat(formatString107)
        
        Submit.setTitle(String.localizedStringWithFormat(formatString106), for: .normal)
        listItem = createArray1()
        listItem2 = createArray2()
        expenseTable.estimatedRowHeight = 350
        expenseTable.rowHeight = UITableView.automaticDimension
        
        }
    
    let formatString20 = NSLocalizedString("Site expenses",
                                           comment: "Site expenses")
    
    let formatString21 = NSLocalizedString("General and administrative expenses",
                                           comment: "General and administrative expenses")
    
    let formatString22 = NSLocalizedString("Gasoline",
                                           comment: "Gasoline")
    
    
    
    let formatString23 = NSLocalizedString("Common Site Expenses",
                                           comment: "Tanaka House Material")
    
    
    let formatString14 = NSLocalizedString("Enter Value",
                                           comment: "Enter Value")
    
    
    let formatString15 = NSLocalizedString("Expenses",
                                           comment: "Expenses")
    
    
    let formatString101 = NSLocalizedString("Save",
                                           comment: "Save")
    
    let formatString102 = NSLocalizedString("Entertainment Expenses",
                                            comment: "Save")
    
    
    let formatString103 = NSLocalizedString("Expandable Expenses",
                                            comment: "Save")
    
    
    let formatString104 = NSLocalizedString("Other Expenses",
                                            comment: "Save")
    
    
    let formatString105 = NSLocalizedString("Welfare Expenses",
                                            comment: "Save")
    
    
    let formatString106 = NSLocalizedString("Submit",
                                            comment: "Save")
    
    let formatString107 = NSLocalizedString("Note",
                                            comment: "Note")
    
    
    @IBOutlet weak var noteTV: UITextView!
    
    
    
    func createArray1() -> [expenselist] {
        var listitem: [expenselist] = []
        
        
        let list1 = expenselist(expensetype: String.localizedStringWithFormat(formatString20), material: String.localizedStringWithFormat(formatString14))
      
        
        listitem.append(list1)
        
        return listitem
    }
    
    func createArray2() -> [expenselist2] {
        var listitem2: [expenselist2] = []
        
        
       
        let list2 = expenselist2(expensetype: String.localizedStringWithFormat(formatString23), material: String.localizedStringWithFormat(formatString14))
       
        
        listitem2.append(list2)
        
        return listitem2
    }
    
    
}
extension expenseTV: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return 1
        default:
            return 0
           
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell5 = UITableViewCell()
        
        
        switch indexPath.section {
            
            
        case 0:
            let listitem = listItem[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "infocell", for: indexPath) as! infocell
            siteexp.text  = cell.textfield.text
            noteTV.text = cell.notetextfield.text
            note = cell.notetextfield.text!
            cell.setcell(expenselist: listitem)
           
            return cell
            
            
        case 1:
            let listitem2 = listItem2[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "infocell2", for: indexPath) as! infocell2
            commsiteexp.text = cell.textfield.text
            cell.setcell(expenselist2: listitem2)
            return cell
            
        case 2:
            let cell2 = tableView.dequeueReusableCell(withIdentifier: "gmexcell", for: indexPath) as! gmexcell
            entertainment.text = cell2.entertainmenttf.text
            expandables.text = cell2.expandablestf.text
            gasoline.text = cell2.gasolinetf.text
            otherexpenses.text = cell2.othertextfield.text
            welfareexp.text = cell2.welfaretf.text
            cell2.heading.text = String.localizedStringWithFormat(formatString21)

            cell2.gaslbl.text = String.localizedStringWithFormat(formatString22)
            
            cell2.enterlbl.text = String.localizedStringWithFormat(formatString102)
            
            cell2.expndbl.text = String.localizedStringWithFormat(formatString103)
            
            
            cell2.othrlbl.text = String.localizedStringWithFormat(formatString104)
            
            
            cell2.wlflbl.text = String.localizedStringWithFormat(formatString105)
            
            
    cell2.savebtn.setTitle(String.localizedStringWithFormat(formatString101), for: .normal)
            return cell2
            
        default:
            print("Default Selected")
            }
        return cell5
    }
        
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
       if selectedIndex == indexPath.section
            {
                if self.iscollapse == false {
                    self.iscollapse = true
                }else{
                    self.iscollapse = false
                }
            }else{
                self.iscollapse = true
            }
            self.selectedIndex = indexPath.section
            tableView.reloadRows(at: [indexPath], with: .automatic)
            }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            if self.selectedIndex == indexPath.section && iscollapse == true{
                return 250
            }else{
                return 50}
            
        case 1:
            if self.selectedIndex == indexPath.section && iscollapse == true{
                return 200
            }else{
                return 50}
            
        case 2:
            if self.selectedIndex == indexPath.section && iscollapse == true{
                return 620
            }else{
                return 50}
            
        default:
            return 50
            }}
    

}

