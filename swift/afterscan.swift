//
//  afterscan.swift
//  RobinApp
//
//  Created by mac on 12/06/19.
//  Copyright © 2019 com.assignment.robin. All rights reserved.
//

import UIKit


class afterscan: UIViewController{
    
    
    let blackview = UIView()
    var vcview : UIView!
    let companyTV = UITableView()
    var detectedcompanyname = ""
    var okadate = ""
    var iscollapse = false
    var selectedindex = 0
    var listItem: [listarray1] = []
    var listItem2: [listarray2] = []
    var companyArray: [String] = []
    let formatString21 = NSLocalizedString("Site expenses",comment: "Site expenses")
    let formatString22 = NSLocalizedString("Tanaka House Material",comment: "Tanaka House Material")
    let formatString23 = NSLocalizedString("Gasoline",comment: "Gasoline")
    let formatString24 = NSLocalizedString("General Management Expenses",comment: "General Management Expenses")
    let formatString20 = NSLocalizedString("Robin",comment: "Robin")
    let formatString17 = NSLocalizedString("Robin management",comment: "Robin management")
    let formatString18 = NSLocalizedString("Robin maintenance",comment: "Robin maintenance")
    let formatString19 = NSLocalizedString("Robin Trust",comment: "Robin Trust")
     let formatString25 = NSLocalizedString("Save",comment: "Save")
    let formatString26 = NSLocalizedString("Cancel",comment: "Cancel")
    
    
    
    
 
    func selectCompany(){
        let formatString15 = NSLocalizedString("Select Company",comment: "Select Company")
        let alert = UIAlertController(title: String.localizedStringWithFormat(formatString15), message: "\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        alert.view.addSubview(companyTV)
        companyTV.frame = CGRect(x: 10, y: 50, width: 250, height: 200)
        companyTV.register(UITableViewCell.self, forCellReuseIdentifier: "CompanyCell")
        let close = UIAlertAction(title: "Close", style: .destructive, handler: nil)
        alert.addAction(close)
        present(alert, animated: true, completion: nil)}
func selectPaymentmode() {
        
        let formatString12 = NSLocalizedString("Payment Pop up",comment: "Payment Pop up")
        let alert = UIAlertController(title: String.localizedStringWithFormat(formatString12), message: "", preferredStyle: .alert)
        
         let formatString13 = NSLocalizedString("Cash",comment: "Cash")
            let cash = UIAlertAction(title: String.localizedStringWithFormat(formatString13), style: .default)
        
        
        let formatString14 = NSLocalizedString("Personal Credit Card",comment: "Personal Credit Card")
            let cc = UIAlertAction(title: String.localizedStringWithFormat(formatString14), style: .default) {
            _ -> Void in
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "payment") as! Payment
            self.present(nextViewController, animated: true, completion: nil)}
            alert.addAction(cash)
            alert.addAction(cc)
           present(alert, animated: true, completion: nil)}
    
    
    func getexpenses(){
        self.view.addSubview(blackview)
        blackview.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        blackview.alpha = 0.8
        blackview.frame = self.view.bounds
        let cross = UIButton()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "expenseTV")as! expenseTV
        self.addChild(vc)
        vcview = vc.view
        self.view.addSubview(vcview)
        
        vcview.frame = CGRect(x: blackview.frame.width/2-150, y: blackview.frame.height/2-150, width: 300, height: 300)
        vcview.addSubview(cross)
        cross.frame = CGRect(x: vcview.frame.width-45, y: 5, width: 40, height: 40)
        cross.setImage(#imageLiteral(resourceName: "cross grey"), for: .normal)
        cross.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
    }
    @objc func buttonAction(sender: UIButton!) {
        blackview.alpha = 0
        vcview.alpha = 0
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        companyArray = [String.localizedStringWithFormat(formatString20),String.localizedStringWithFormat(formatString17),String.localizedStringWithFormat(formatString18),String.localizedStringWithFormat(formatString19)]
        listItem = createArray()
        listItem2 = createmoreArray()
        T1.delegate = self
        T1.dataSource = self
        T2.delegate = self
        T2.dataSource = self
       companyTV.delegate = self
        companyTV.dataSource = self
        }
    
    func createArray() -> [listarray1] {
        var listitem: [listarray1] = []
        
        let formatString1 = NSLocalizedString("Company Name",comment: "Company Name")
        let formatString2 = NSLocalizedString(detectedcompanyname,comment: "Robin")
        let formatString3 = NSLocalizedString("Oka date",comment: "Oka date")
        let formatString4 = NSLocalizedString("Store",comment: "Store")
        let formatString5 = NSLocalizedString("Payment",comment: "Payment")
        let formatString6 = NSLocalizedString("individual kilijiri Mountain",comment: "individual kilijiri Mountain")
        let formatString7 = NSLocalizedString("Payment destination",comment: "Payment destination")
        let formatString8 = NSLocalizedString("Seven-Eleven",comment: "Seven-Eleven")
        let formatString9 = NSLocalizedString("Amount(tax included)",comment: "Amount(tax included)")
        let list1 = listarray1(image: #imageLiteral(resourceName: "1"), title: String.localizedStringWithFormat(formatString1), value: String.localizedStringWithFormat(formatString2))
        let list2 = listarray1(image: #imageLiteral(resourceName: "2"), title: String.localizedStringWithFormat(formatString3), value: okadate)
        let list3 = listarray1(image: #imageLiteral(resourceName: "3"), title: String.localizedStringWithFormat(formatString4), value: String.localizedStringWithFormat(formatString4))
        let list4 = listarray1(image: #imageLiteral(resourceName: "4"), title: String.localizedStringWithFormat(formatString5), value: String.localizedStringWithFormat(formatString6))
        let list5 = listarray1(image: #imageLiteral(resourceName: "3229640-64"), title: String.localizedStringWithFormat(formatString7), value: String.localizedStringWithFormat(formatString8))
        let list6 = listarray1(image: #imageLiteral(resourceName: "6"), title: String.localizedStringWithFormat(formatString9), value: "3000")
       
        
        listitem.append(list1)
        listitem.append(list2)
        listitem.append(list3)
        listitem.append(list4)
        listitem.append(list5)
        listitem.append(list6)
        
        
        return listitem
        
    }
    func createmoreArray() -> [listarray2] {
        var listitem2: [listarray2] = []
        let formatString10 = NSLocalizedString("food",comment: "food")
        let list1 = listarray2(food: String.localizedStringWithFormat(formatString10), price: "200")
        let list2 = listarray2(food: String.localizedStringWithFormat(formatString10), price: "50")
        let list3 = listarray2(food: String.localizedStringWithFormat(formatString10), price: "175")
        let list4 = listarray2(food: String.localizedStringWithFormat(formatString10), price: "35")
        let list5 = listarray2(food:String.localizedStringWithFormat(formatString10), price: "10")
        listitem2.append(list1)
        listitem2.append(list2)
        listitem2.append(list3)
        listitem2.append(list4)
        listitem2.append(list5)
        return listitem2
}
}

extension afterscan : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == T1){
            let listitem = listItem[indexPath.row]
            let Cell1 = tableView.dequeueReusableCell(withIdentifier: "Cell1")as! Cell1
            Cell1.setCell1(listarray1: listitem)
            return Cell1
        }
        else if tableView == T2{
            let listitem = listItem2[indexPath.row]
            let Cell2 = tableView.dequeueReusableCell(withIdentifier: "Cell2")as! Cell2
            
            if (indexPath.row == 0||indexPath.row == 2||indexPath.row == 3||indexPath.row == 4) {  Cell2.arrow.image = #imageLiteral(resourceName: "iconfinder_arrow-right-01_186409")}
            Cell2.setcell2(listarray2: listitem)
            return Cell2
        }else {
            let CompanyCell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell")
            CompanyCell?.textLabel!.text = companyArray[indexPath.row]
            let rightArr = UIImageView()
            rightArr.image = #imageLiteral(resourceName: "iconfinder_arrow-right-01_186409")
            CompanyCell?.addSubview(rightArr)
            rightArr.frame = CGRect(x: 230, y: 15, width: 20, height: 20)
            return CompanyCell!
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == T1{ return listItem.count}
        else if tableView == T2{return listItem2.count}else {return companyArray.count}}
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == T1{
            return 50
        }else if tableView == T2{
            return 40
        }else {return 50}}
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == T1{
            if indexPath.row == 0{selectCompany()}
            if indexPath.row == 3{selectPaymentmode()}
        }else if tableView == T2{if indexPath.row == 0{getexpenses()}}}
    
}
