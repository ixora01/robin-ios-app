
import Foundation
import UIKit

class listarray1 {
    var image: UIImage
    var title: String
    var value: String
    
    init(image: UIImage, title: String, value: String){
        self.image = image
        self.title = title
        self.value = value
    }
}
