//
//  expenseTV.swift
//  Robin
//
//  Created by mac on 09/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit

class expenseTV: UIViewController {
    @IBOutlet weak var siteexpense: UILabel!
    @IBOutlet weak var expenseTable: UITableView!
    @IBAction func cross(_ sender: Any) {
        
    }
    
    var listItem: [expenselist] = []
    var iscollapse: Bool = false
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatString14 = NSLocalizedString("Site Expense",
                                               comment: "Site Expense")
        
        
        
        siteexpense.text = String.localizedStringWithFormat(formatString14)
        
        listItem = createArray1()
        expenseTable.estimatedRowHeight = 350
        expenseTable.rowHeight = UITableView.automaticDimension
        
        
        
    }
    
    let formatString20 = NSLocalizedString("Site expenses",
                                           comment: "Site expenses")
    
    let formatString21 = NSLocalizedString("General Management expenses",
                                           comment: "General Management expenses")
    
    let formatString22 = NSLocalizedString("Gasoline",
                                           comment: "Gasoline")
    
    
    let formatString23 = NSLocalizedString("Tanaka House Material",
                                           comment: "Tanaka House Material")
    
    
    
    
    func createArray1() -> [expenselist] {
        var listitem: [expenselist] = []
        
        
        let list1 = expenselist(expensetype: String.localizedStringWithFormat(formatString20), material: String.localizedStringWithFormat(formatString23))
        let list2 = expenselist(expensetype: String.localizedStringWithFormat(formatString21), material: String.localizedStringWithFormat(formatString22))
        listitem.append(list1)
        listitem.append(list2)
        return listitem
    }
    
    
}
extension expenseTV: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let listitem = listItem[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "infocell", for: indexPath) as! infocell
        cell.setcell(expenselist: listitem)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectedIndex == indexPath.row && iscollapse == true{
            return 200
        }else{
            return 50
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if selectedIndex == indexPath.row
        {
            if self.iscollapse == false {
                self.iscollapse = true
            }else{
                self.iscollapse = false
            }
        }else{
            self.iscollapse = true
        }
        self.selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    
    
}
