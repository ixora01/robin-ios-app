//
//  Payment.swift
//  RobinApp
//
//  Created by mac on 12/06/19.
//  Copyright © 2019 com.assignment.robin. All rights reserved.
//

import UIKit

class Payment: UIViewController {

    @IBOutlet weak var paynow: UIButton!
    @IBOutlet weak var cardno: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardno.clipsToBounds = true
        cardno.layer.cornerRadius = 5
        paynow.clipsToBounds = true
        paynow.layer.cornerRadius = 5
        
        let formatString = NSLocalizedString("Card no.",
                                             comment: "Card no.")
        
        
        let formatString1 = NSLocalizedString("Pay Now",
                                             comment: "Pay Now")
        
        cardno.text = String.localizedStringWithFormat(formatString)
        
        paynow.setTitle(String.localizedStringWithFormat(formatString1), for: .normal)
        }
    

    

}
