
import UIKit

class create_Account: UIViewController {

    @IBOutlet weak var femalebtn: UIButton!
    
    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var signup: UIButton!
    @IBOutlet weak var malebtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatString = NSLocalizedString("Sign Up",
                                             comment: "Sign Up")
        
        
        let formatString1 = NSLocalizedString("email ID",
                                             comment: "email ID")
        
        emailID.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString1))

            signup.setTitle(String.localizedStringWithFormat(formatString), for: .normal)
        

       
    }
    
    @IBAction func clickmale(_ sender: Any) {
        
        if femalebtn.isSelected {
            femalebtn.isSelected = false
            malebtn.isSelected = true
        }else{
            malebtn.isSelected = true
        }
    }
    
    @IBAction func clickfemale(_ sender: Any) {
        
        if malebtn.isSelected {
            malebtn.isSelected = false
            femalebtn.isSelected = true
        }else{
            femalebtn.isSelected = true
        }
    }
}
