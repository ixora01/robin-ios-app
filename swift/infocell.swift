//
//  infocell.swift
//  RobinApp
//
//  Created by mac on 05/07/19.
//  Copyright © 2019 com.assignment.robin. All rights reserved.
//

import UIKit

class infocell: UITableViewCell {

    @IBOutlet weak var expensetype: UILabel!
    
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var material: UILabel!

    @IBOutlet weak var savebutton: UIButton!
    
    @IBOutlet weak var cancelbutton: UIButton!
    
    func setcell(expenselist : expenselist){
        expensetype.text = expenselist.expensetype
        material.text = expenselist.material
        textfield.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        textfield.layer.borderWidth = 1
        
    }

}
