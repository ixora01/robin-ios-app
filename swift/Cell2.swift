//
//  Cell2.swift
//  RobinApp
//
//  Created by mac on 12/06/19.
//  Copyright © 2019 com.assignment.robin. All rights reserved.
//

import UIKit

class Cell2: UITableViewCell {

    @IBOutlet weak var food: UILabel!
    
    @IBOutlet weak var foodpricevalue: UILabel!
    
    @IBOutlet weak var arrow: UIImageView!
    
    
    
    func setcell2(listarray2 : listarray2){
        food.text = listarray2.food
        foodpricevalue.text = listarray2.price
    }
    
    
    
}
