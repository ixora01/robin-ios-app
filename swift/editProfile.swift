//
//  editProfile.swift
//  Dr plus
//
//  Created by mac on 11/06/19.
//  Copyright © 2019 com.assignment.auction. All rights reserved.
//

import UIKit

class editProfile: UIViewController {
    @IBOutlet weak var ProfileImage: UIImageView!
    
    @IBOutlet weak var fullname: UITextField!
    
    @IBOutlet weak var Changepassword: UITextField!
    
    @IBOutlet weak var emailid: UITextField!
    
    @IBOutlet weak var phone: UITextField!
    
    
    
    
    
    @IBAction func clickfemale(_ sender: Any) {
        if malebtn.isSelected {
            malebtn.isSelected = false
            femalebtn.isSelected = true
        }else{
            femalebtn.isSelected = true
        }
    }
    @IBOutlet weak var femalelbl: UILabel!
    @IBAction func clickmale(_ sender: UIButton) {
        if femalebtn.isSelected {
            femalebtn.isSelected = false
            malebtn.isSelected = true
        }else{
            malebtn.isSelected = true
        }
        
    }
    @IBOutlet weak var genderlbl: UILabel!
    @IBOutlet weak var femalebtn: UIButton!
    @IBOutlet weak var malebtn: UIButton!
    @IBOutlet weak var Plusicon: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let formatString = NSLocalizedString("female",
                                             comment: "female")
        
        let formatString2 = NSLocalizedString("gender",
                                            comment: "gender")
        
        let formatString3 = NSLocalizedString("full name",
                                              comment: "full name")
        
        fullname.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString3))
        
        let formatString4 = NSLocalizedString("Phone number",
                                              comment: "Phone number")
        
        phone.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString4))
        
        
        
        
        
        femalelbl.text = String.localizedStringWithFormat(formatString)
        genderlbl.text = String.localizedStringWithFormat(formatString2)
        
        
       ProfileImage.clipsToBounds=true
        ProfileImage.layer.cornerRadius = ProfileImage.frame.width/2
        Plusicon.clipsToBounds=true
        Plusicon.layer.cornerRadius = 10
        
    }
    

    
   


}
