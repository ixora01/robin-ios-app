import UIKit
import MobileCoreServices
import TesseractOCR



class homePage: UIViewController {
     var newString = ""
    var newString1 = ""
    var str = ""
    var open : Bool = true
    let a = UIView()
    var nameText = ""
    
    @IBOutlet weak var cameraview: UIImageView!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var TextField: UITextField!
    @IBOutlet weak var backbutton: UIButton!
    @IBOutlet weak var L2: UILabel!
    @IBOutlet weak var L1: UILabel!
    @IBOutlet weak var subview2image: UIImageView!
    
    @IBOutlet weak var subview3image: UIImageView!
    @IBOutlet weak var subsideview2: UIView!
    
    @IBOutlet weak var subsideview3: UIView!
    @IBOutlet weak var baseview: UIView!
    @IBOutlet weak var subImage: UIImageView!
    @IBOutlet weak var subsideview1: UIView!
    @IBOutlet weak var mainsideview: UIView!
    @IBAction func tapSidebar(_ sender: Any) {
        
        
        if (open == true){
        UIView.animate(withDuration: 0.5,  animations: {
            self.mainsideview.frame = CGRect(x: 0, y: 80, width: self.view.frame.width/2, height: self.view.frame.height-80)
        }, completion: nil)
            open = false
        }else if (open == false){
            UIView.animate(withDuration: 0.5, animations: {
                self.mainsideview.frame = CGRect(x: 0-self.view.frame.width/2, y: 80, width: self.view.frame.width/2, height: self.view.frame.height-80)
            }, completion: nil)
            
            
            open = true
        }
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatString2 = NSLocalizedString("Order History",
                                             comment: "Order history label")
        
        let formatString3 = NSLocalizedString("Log Out",
                                              comment: "log out")
        
        L1.text = String.localizedStringWithFormat(formatString2)
        L2.text = String.localizedStringWithFormat(formatString3)
        
        
        
        
        
        cameraview.clipsToBounds = true
        cameraview.layer.cornerRadius = 10
        view.addSubview(mainsideview)
        mainsideview.frame = CGRect(x: 0-self.view.frame.width/2, y: 80, width: self.view.frame.width/2, height: self.view.frame.height-80)
        mainsideview.addSubview(subsideview1)
        subsideview1.frame = CGRect(x: 0, y: 0, width: mainsideview.frame.width, height: mainsideview.frame.width)
        subsideview1.addSubview(subImage)
        subImage.frame = CGRect(x:15 , y:10 , width: subsideview1.frame.width-35, height: subsideview1.frame.height-30)
        subImage.clipsToBounds = true
        subImage.layer.cornerRadius = subImage.frame.width/2
        mainsideview.addSubview(subsideview2)
        mainsideview.addSubview(subsideview3)
        subsideview2.frame = CGRect(x: 0, y: mainsideview.frame.width, width: mainsideview.frame.width, height: 50)
        subsideview3.frame = CGRect(x: 0, y: mainsideview.frame.width+50, width: mainsideview.frame.width, height: 50)
        subsideview2.addSubview(subview2image)
        subsideview3.addSubview(subview3image)
        subview2image.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        subview3image.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        subsideview2.addSubview(L1)
        subsideview3.addSubview(L2)
        L1.frame = CGRect(x: 55, y: subsideview2.frame.height/2-10, width: subsideview2.frame.width-55, height: 20)
        L2.frame = CGRect(x: 55, y: subsideview3.frame.height/2-10, width: subsideview3.frame.width-55, height: 20)
        
        let editprofilelbl = UILabel()
        subsideview1.addSubview(editprofilelbl)
       
        let formatString = NSLocalizedString("Edit Profile",
                                             comment: "Edit Profile")
        editprofilelbl.text = String.localizedStringWithFormat(formatString)
        editprofilelbl.frame = CGRect(x: 0, y: subsideview1.frame.height-30, width: subsideview1.frame.width, height: 20)
        editprofilelbl.textAlignment = NSTextAlignment.center
        editprofilelbl.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
    }
    
    @IBAction func takePic(_ sender: Any) {getCamera()}
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBAction func takephoto(_ sender: Any){getCamera() }
    
    @IBAction func indicator(_ sender: Any) {
        
        let rotar = UIActivityIndicatorView()
        self.view.addSubview(rotar)
        rotar.frame = CGRect(x: 0, y:  0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        rotar.startAnimating()
        rotar.hidesWhenStopped = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "purchaseHistory")as! purchaseHistory
        show(vc, sender: self)
        
    }
    
  func getCamera(){
        let imagePickerActionSheet =
            UIAlertController(title: "Snap/Upload Image",
                              message: nil,
                              preferredStyle: .actionSheet)
        
        // 2
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let formatString6 = NSLocalizedString("Take Photo",comment: "Take Photo")
            let cameraButton = UIAlertAction(title: String.localizedStringWithFormat(formatString6),
                style: .default) { (alert) -> Void in
                    // 1
                    self.activityIndicator.startAnimating()
                    // 2
                    let imagePicker = UIImagePickerController()
                    // 3
                    imagePicker.delegate = self
                    // 4
                    imagePicker.sourceType = .camera
                    // 5
                    imagePicker.mediaTypes = [kUTTypeImage as String]
                    // 6
                    self.present(imagePicker, animated: true, completion: {
                        // 7
                        self.activityIndicator.stopAnimating()
                    })

            }
            imagePickerActionSheet.addAction(cameraButton)
        }
    
    
    let formatString4 = NSLocalizedString("Choose Existing",
                                         comment: "Choose Existing")
        
        let libraryButton = UIAlertAction(
            title: String.localizedStringWithFormat(formatString4),
            style: .default) { (alert) -> Void in
                self.activityIndicator.startAnimating()
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.mediaTypes = [kUTTypeImage as String]
                self.present(imagePicker, animated: true, completion: {
                    self.activityIndicator.stopAnimating()
                })

        }
        imagePickerActionSheet.addAction(libraryButton)
    let formatString7 = NSLocalizedString("Cancel",comment: "Cancel")
        let cancelButton = UIAlertAction(title: String.localizedStringWithFormat(formatString7), style: .cancel)
        imagePickerActionSheet.addAction(cancelButton)
        present(imagePickerActionSheet, animated: true)

    }
    
    
    
    
    
    func performImageRecognition(_ image: UIImage) {
        
        let scaledImage = image.scaledImage(1000) ?? image

        
        // 1
        if let tesseract = G8Tesseract(language: "jpn") {
            // 2
            tesseract.engineMode = .tesseractCubeCombined
            // 3
            tesseract.pageSegmentationMode = .auto
            // 4
            tesseract.image = scaledImage
            // 5
            tesseract.recognize()
            // 6
            nameText = tesseract.recognizedText!
            
            str = tesseract.recognizedText!
            let start = str.index(str.startIndex, offsetBy: 0)
            let end = str.index(str.startIndex, offsetBy: 7)
            let result = str[start..<end] // The result is of type Substring
            newString = String(result)
            
            
            
            let start1 = str.index(str.endIndex, offsetBy: -10)
            let end1 = str.index(str.endIndex, offsetBy: 0)
            let result1 = str[start1..<end1] // The result is of type Substring
            newString1 = String(result1)
            
            
            
            
           
            
            
            
            
            
            
            //performSegue(withIdentifier: "transfer", sender: self)
            
           performSegue(withIdentifier: "ocr", sender: self)
           }
        activityIndicator.stopAnimating()
        }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transfer"{ let vc = segue.destination as! afterscan
            vc.detectedcompanyname = newString
            vc.okadate = newString1
        }
        if segue.identifier == "ocr"{let vc = segue.destination as! Scnner
            vc.finalName = str}
       }
    
  

}

extension homePage: UINavigationControllerDelegate {
    
    
}
extension homePage: UIImagePickerControllerDelegate {
    // 3
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // 1
        guard let selectedPhoto =
            info[.originalImage] as? UIImage else {
                dismiss(animated: true)
                return
        }
        // 2
        activityIndicator.startAnimating()
        // 3
        dismiss(animated: true) {
            self.performImageRecognition(selectedPhoto)
        }

    }
    
    
    
    
}

// MARK: - UIImage extension

//1
extension UIImage {
    // 2
    func scaledImage(_ maxDimension: CGFloat) -> UIImage? {
        // 3
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        // 4
        if size.width > size.height {
            scaledSize.height = size.height / size.width * scaledSize.width
        } else {
            scaledSize.width = size.width / size.height * scaledSize.height
        }
        // 5
        UIGraphicsBeginImageContext(scaledSize)
        draw(in: CGRect(origin: .zero, size: scaledSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        // 6
        return scaledImage
    }
}



