//
//  loginPage.swift
//  Robin
//
//  Created by mac on 09/07/19.
//  Copyright © 2019 com.assignment.ixorainfotech. All rights reserved.
//

import UIKit

class loginPage: UIViewController {

    @IBOutlet weak var signup: UIButton!
    @IBOutlet weak var loginbutton: UIButton!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var fullname: UITextField!
    @IBOutlet weak var loginImage: UIImageView!
    @IBOutlet weak var chooselanguage: UILabel!
    
    @IBOutlet weak var clickenglish: UIButton!
    
    @IBOutlet weak var clickjapanese: UIButton!
    
    
    
    @IBAction func getEnglish(_ sender: Any) {
        languageButtonAction2()
        
        
    }
    
    @IBAction func getJapanese(_ sender: Any) {
        languageButtonAction()
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatString9 = NSLocalizedString("Login",
                                              comment: "Login")
        loginbutton.setTitle(String.localizedStringWithFormat(formatString9), for: .normal)
        
        let formatString10 = NSLocalizedString("Sign Up",
                                              comment: "Sign Up")
        signup.setTitle(String.localizedStringWithFormat(formatString10), for: .normal)
        
        let formatString5 = NSLocalizedString("Choose language",
                                              comment: "Choose language")
        chooselanguage.text = String.localizedStringWithFormat(formatString5)
        loginImage.clipsToBounds = true
        loginImage.layer.cornerRadius = self.loginImage.frame.width/2
        loginImage.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        loginImage.layer.borderWidth = 1.0
        let formatString = NSLocalizedString("full name",
                                             comment: "full name")
        fullname.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString),
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray ])
        let formatString2 = NSLocalizedString("Password",
                                              comment: "Password")
        
        
        Password.attributedPlaceholder = NSAttributedString(string: String.localizedStringWithFormat(formatString2),
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray ])}

       
    

   
}
func languageButtonAction() {
    // This is done so that network calls now have the Accept-Language as "hi" (Using Alamofire) Check if you can remove these
    UserDefaults.standard.set(["ja"], forKey: "AppleLanguages")
    UserDefaults.standard.synchronize()
    
    // Update the language by swaping bundle
    Bundle.setLanguage("ja")
    
    // Done to reintantiate the storyboards instantly
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()}

func languageButtonAction2() {
    // This is done so that network calls now have the Accept-Language as "hi" (Using Alamofire) Check if you can remove these
    UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
    UserDefaults.standard.synchronize()
    
    // Update the language by swaping bundle
    Bundle.setLanguage("en")
    
    // Done to reintantiate the storyboards instantly
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    UIApplication.shared.keyWindow?.rootViewController = storyboard.instantiateInitialViewController()
}
    
