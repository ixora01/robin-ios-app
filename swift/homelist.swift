
import Foundation
import UIKit

class homelist {
    var icon: UIImage
    var name: String
    
    init(icon: UIImage, name: String){
        self.icon = icon
        self.name = name
    }
}
