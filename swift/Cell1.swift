//
//  Cell1.swift
//  RobinApp
//
//  Created by mac on 12/06/19.
//  Copyright © 2019 com.assignment.robin. All rights reserved.
//

import UIKit

class Cell1: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var value: UILabel!
    
    func setCell1(listarray1 : listarray1) {
        
        icon.image = listarray1.image
        title.text = listarray1.title
        value.text = listarray1.value
        
    }
    
    
}
